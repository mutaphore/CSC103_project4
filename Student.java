//CPE103 - Gharibyan - Project 4
//Andy Chen - dchen31@calpoly.edu
//Michael Woodson - mwoodson@calpoly.edu
//
//Student Class:
//Represent one student’s information record 
//which contains the student's ID and last name

import java.math.*;

public class Student {
	
	private long id;
	private String lastName;
	
	//Constructor - sets the student ID and last name
	public Student(long id, String lastName) {
		
		this.id = id;
		this.lastName = lastName;
		
	}
	
	//Compares the current instance of Student with another instance
	//passed in as 'other' and return's true if their ID's match,
	//false otherwise.
	public boolean equals(Object other) {
		
		return this.id == ((Student)other).id;
		
	}
	
	//Creates a string containing the student's information formatted as:
	//{ id: 11054, name: Smith }
	public String toString() {
		
		String temp = "";
		
		temp = temp.concat("{ id: " + String.valueOf(id) + ", name: " + lastName + " }");
		
		return temp;
		
	}
	
	//To return the hash code of the key of this object. The student 
	//id is used as a key in hashing
	public int hashCode() {
		
		Long temp = id;
		
		return temp.hashCode();
		
	}
	
}