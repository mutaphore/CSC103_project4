//CPE103 - Gharibyan - Project 4
//Andy Chen - dchen31@calpoly.edu
//Michael Woodson - mwoodson@calpoly.edu
//
//HTDriver Class:
//Client class that tests the functionality of the HashTable
//class and Student class.

import java.util.*;
import java.io.*;

public class HTDriver {
	
	public static void main(String[] args) {
		
		//Get input file name from user
		String inputFileName;
		Scanner input = new Scanner(System.in);		
		System.out.print("Enter input file name: ");
		inputFileName = input.next();
		input.nextLine();
		File reader = new File(inputFileName);
		char choice = 'z';
		
		//Temp variables
		Scanner lineScanner;	//Saves a line for parsing
		int N;
		Long tempID;
		String tempName;
		Student tempStudent;
		
		try {
			
			//Input student ID and name info from file
			Scanner fileIn = new Scanner(reader); 
			N = fileIn.nextInt();			
			fileIn.nextLine();
			HashTable studentTable = new HashTable(N);
        
			for(int i=0; i<N; i++) {
				lineScanner = new Scanner(fileIn.nextLine());
				//Make sure the first token is student ID and it's > 0
				if(lineScanner.hasNextLong() && (tempID = lineScanner.nextLong()) > 0) {
					if(lineScanner.hasNext()) {
						tempName = lineScanner.next();
						//Make sure no more than 2 tokens per line or skip
						if(!lineScanner.hasNext())
							studentTable.insert(new Student(tempID,tempName));
					}               
				}
	        	lineScanner.close();
			}
    	
			System.out.print("Choose one of the following operations by entering provided letter:\n" +
							 "	a - add the element \n" +
							 "	d - delete the element \n" +
						     "	f - find and retrieve the element \n" +
							 "	n - get the number of elements in the collection \n" + 
						     "	e - check if the collection is empty \n" +
						     "	k - make the hash table empty \n" +
							 "	p - print the content of the hash table \n" +
						     "	o - output the elements of the collection \n" +
						     "	q - Quit the program \n");
				
			while(choice != 'q')
			{
				
				System.out.println("Enter choice: ");
				choice = input.nextLine().charAt(0);

				switch (choice)
		        {
		        	case 'a':
	
					System.out.print("Enter ID and last name of student to be added: ");
					lineScanner = new Scanner(input.nextLine());
					if(lineScanner.hasNextLong() && (tempID = lineScanner.nextLong()) > 0) {
						if(lineScanner.hasNext()) {
							tempName = lineScanner.next();
							if(!lineScanner.hasNext()) {
								studentTable.insert(new Student(tempID,tempName));		//Create dummy student with tempID
								System.out.println("Student added");
							}
							else
								System.out.println("Invalid entry");
						}
						else
							System.out.println("Invalid entry");
					}
					else
						System.out.println("Invalid entry");
					break;
				
		        	case 'd':
	
					System.out.print("Enter ID of student to be deleted: ");
					lineScanner = new Scanner(input.nextLine());
					if(lineScanner.hasNextLong() && (tempID = lineScanner.nextLong()) > 0) {
						if(!lineScanner.hasNext()) {
								studentTable.delete(new Student(tempID,"Dummy"));		//Create dummy student with tempID
								System.out.println("Student deleted");
						}
						else
							System.out.println("Invalid entry");
					}
					else
						System.out.println("Invalid entry");		
					break;
				
		        	case 'f':
				
					System.out.print("Enter ID of student to retrieve: ");
					lineScanner = new Scanner(input.nextLine());
					if(lineScanner.hasNextLong() && (tempID = lineScanner.nextLong()) > 0) {
						if(!lineScanner.hasNext()) {		
								tempStudent = (Student)studentTable.find(new Student(tempID,"Dummy"));		//Create dummy student with tempID
								if(tempStudent == null)
									System.out.println("Student not found");
								else
									System.out.print("Student found \n" + tempStudent + "\n");
						}
						else
							System.out.println("Invalid entry");
					}
					else
						System.out.println("Invalid entry");								
					break;
	
		        	case 'n':					
					System.out.println("Number of elements in collection: " + studentTable.elementCount());						
					break;
				
		        	case 'e':
					if(studentTable.isEmpty())
						System.out.println("Collection is empty");
					else
						System.out.println("Collection is NOT empty");
					break;
				
		        	case 'k':
					studentTable.makeEmpty();
					System.out.println("Collection contents cleared");
					break;
				
		        	case 'p':
					studentTable.printTable();					
					break;
				
		        	case 'o':
					Iterator sIter = studentTable.iterator();
					while(sIter.hasNext())
						System.out.println((Student)sIter.next());
					break;
				
		        	case 'q':
					System.out.println("Quitting!");
					break;
				
		        	default:
					System.out.println("Invalid choice!");
					break;
				
				}
			
			}	
		
		}catch(IOException e) {
			System.out.println("File not found!");
		}
			
	}

}