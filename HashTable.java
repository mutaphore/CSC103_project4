//CPE103 - Gharibyan - Project 4
//Andy Chen - dchen31@calpoly.edu
//Michael Woodson - mwoodson@calpoly.edu
//
//HashTable class:
//Represents a generic Hash Table ADT with
//open addressing and quadratic probing

import java.util.*;

public class HashTable {
	
	//HashEntry Class
	private class HashEntry {

		public Object element;
		public boolean isActive;
		
		public HashEntry(Object data) {
			
			element = data;
			isActive = true;
			
		}
		
	}
	
	//HashTable private data members
	private HashEntry[] table;
	private int occupiedCells;
	
	//HashTable constructor that takes in the number of elements
	//as a parameter and creates a hash table that has size of at least
	//twice the number of elements and is a prime number
	public HashTable (int numElements) {
		
		table = new HashEntry[nextPrime(numElements*2)];
		occupiedCells = 0;

	}
	
	//Finds and returns the next prime number that is greater 
	//than or equal to the parameter num.
	private int nextPrime(int num) {
		
		boolean isPrime = false;
		int prime = 0;
				
		for(int i=num; isPrime == false; i++) {
			
			isPrime = true;
			
			for(int j=2; j<i; j++) {				
				if(i % j == 0) {		//Check to see if there is a factor (other than 1 and itself)
					j = i;				//Exit inner loop (not prime)						
					isPrime = false;
				}
			}
			
			if(isPrime == true) 		//Exit outer loop if exactly 2 factors
				prime = i;
		
		}
		
		return prime;
		
	}
	
	//Iterator class that traverses the elements of the collection
	private class Iter implements Iterator {
		
		private int cursor;
		
		public Iter() {
			
			cursor = 0;
			moveCursor();
		
		}
		
		//Support method for next and initialization.
		//Move cursor to an element in the collection that is active
		private void moveCursor() {
			
			boolean found = false;

			while(cursor < table.length && found == false) {
				if(table[cursor] != null) {
					if(table[cursor].isActive == true)
						found = true;
					else
						cursor++;
				}
				else
					cursor++;
			}
		}
		
		public boolean hasNext(){
         
	    	if(cursor >= table.length)
	        	return false;
	     	else
	        	return true;
 
		}
		
		//Returns the current element the cursor is pointing at and
		//increments cursor to the next active non-empty element in table
		public Object next() {
			
			HashEntry temp;
			
			if(cursor >= table.length)
				throw new NoSuchElementException();
			else {
			 	temp = table[cursor];
				cursor++;
				moveCursor();
			}
			
			return temp.element;
			
		}
		
		public void remove(){
         
		         throw new UnsupportedOperationException();
         
		}
		
	}
	
	//Finds the hash code for the input object data value
    private int hash(Object x) {
      
       return Math.abs(x.hashCode() % table.length);
      
    }
	
	//Support method for insert, delete, and find.
	//Returns the location of the the given x element if x is found in the
	//table (active or inactive); if x is not in the table, it returns the 
	//location (that currently contains null) where x should be placed.
	private int findPosition(Object x) {
		
		int i = 0;
		int hashValue = hash(x);
		int index = hashValue;
		
		while(table[index] != null && !table[index].element.equals(x)) {
			i++;
			index = ( hashValue + i*i ) % table.length;		//quadratic probe
		}
		
		return index;
		
	}
	
	//Creates a new table of prime size that is at least twice as large
	//as the old table, and inserts all active elements of the old array
	//into the new one.
	private void rehash() {
		
		HashEntry[] temp = table;
		table = new HashEntry[nextPrime(temp.length*2)]; 
		occupiedCells = 0;
		int index;
		
		for(int i=0; i<temp.length; i++) {
			if(temp[i] != null && temp[i].isActive == true) {
				index = findPosition(temp[i].element);
				table[index] = temp[i];
				occupiedCells++;
			}
		}
		
	}
	
	//Add to the Hash table an entry containing item. Nothing is done if
	//item is already in the table and is active.
	public void insert(Object item) {
		
		int index = findPosition(item);
		
		if(table[index] == null) {
			table[index] = new HashEntry(item);
			occupiedCells++;
			if(occupiedCells >= table.length/2)
				rehash();
		}
		else {
			if(table[index].isActive == false)		//if entry is inactive, set it back to active
				table[index].isActive = true;			
		}
		
	}
	
	//Delete an entry containing item from the hash table.
	public void delete(Object item) {
		
		int index = findPosition(item);
		
		if(table[index] != null && table[index].isActive == true)
			table[index].isActive = false;
		
	}
	
	//Find and retrieve the given element if found, return null if not found
	public Object find(Object item) {
		
		int index = findPosition(item);
		
		if(table[index] != null && table[index].isActive == true)
			return table[index].element;
		else
			return null;
		
	}
	
	//Return the number of elements in the collection (active entries)
	public int elementCount() {
		
		int count = 0;
		
		for(int i=0; i<table.length; i++) {
			if(table[i] != null && table[i].isActive == true)	
				count++;
		}
		
		return count;
		
	}
	
	//Check to see if the collection is empty or not (no more active entries)
	public boolean isEmpty() {
		
		for(int i=0; i<table.length; i++) {
			if(table[i] != null && table[i].isActive == true)	
				return false;
		}
		
		return true;
		
	}
	
	//To make the table empty and clear contents (No occupied cells in table)
	public void makeEmpty() {
		
		for(int i=0; i<table.length; i++) {
			table[i] = null;
		}
		occupiedCells = 0; 
		
	}
	
	//Prints the table content, including unoccupied cells and cells with inactive entries
	public void printTable() {
		
		for(int i=0; i<table.length; i++) {
			System.out.print("[" + i + "]: ");
			if(table[i] == null)
				System.out.print("empty\n");
			else {
				System.out.print(table[i].element);
				if(table[i].isActive)
					System.out.print(", active\n");
				else
					System.out.print(", inactive\n");
			}	
		}
		
	}
	
	//Returns a new Iter object to the caller
	public Iterator iterator() {
		
		return new Iter();
		
	}

}